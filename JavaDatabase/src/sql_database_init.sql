-- Run this file to create database "headhunters"
-- Create the database
CREATE DATABASE IF NOT EXISTS headhunters DEFAULT CHARSET utf8 COLLATE utf8_unicode_ci;

-- Create all tables for the database
USE headhunters;

CREATE TABLE IF NOT EXISTS client (
    client_id SERIAL,
    name CHAR(50) NOT NULL,
    phone CHAR(50) NOT NULL,
	email CHAR(50) NOT NULL,
	username CHAR(16) UNIQUE NOT NULL,
	password CHAR(16) NOT NULL,
	PRIMARY KEY(client_id) );

CREATE TABLE IF NOT EXISTS civilian (
	civ_id BIGINT UNSIGNED UNIQUE NOT NULL,
	PRIMARY KEY(civ_id),
	FOREIGN KEY(civ_id) 
		REFERENCES client(client_id)
		ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS corporation (
	corp_id BIGINT UNSIGNED UNIQUE NOT NULL,
	listings_count BIGINT UNSIGNED DEFAULT 0,
	PRIMARY KEY(corp_id),
	FOREIGN KEY(corp_id)
		REFERENCES client(client_id)
		ON DELETE CASCADE );
		
CREATE TABLE IF NOT EXISTS headhunters (
	id SERIAL,
	city CHAR(50) NOT NULL,
	zip CHAR(16) NOT NULL,
	street CHAR(50) NOT NULL,
	total BIGINT UNSIGNED DEFAULT 0,
	total_filled BIGINT UNSIGNED DEFAULT 0,
        balance DECIMAL(10, 2) DEFAULT 0,
	PRIMARY KEY(id) );

CREATE TABLE IF NOT EXISTS listing (
	listing_id SERIAL,
	filled SMALLINT UNSIGNED DEFAULT 0,
	PRIMARY KEY(listing_id) );
	
CREATE TABLE IF NOT EXISTS corp_listing (
	listing_id BIGINT UNSIGNED NOT NULL,
	corp_id BIGINT UNSIGNED NOT NULL,
	city CHAR(50) NOT NULL,
	deadline DATE NOT NULL,
        hours SMALLINT UNSIGNED NOT NULL,
	PRIMARY KEY(listing_id, corp_id),
	FOREIGN KEY(listing_id)
		REFERENCES listing(listing_id)
		ON DELETE CASCADE,
	FOREIGN KEY(corp_id)
		REFERENCES corporation(corp_id)
		ON DELETE CASCADE );
		
CREATE TABLE IF NOT EXISTS civ_listing (
	listing_id BIGINT UNSIGNED NOT NULL,
	civ_id BIGINT UNSIGNED NOT NULL,
	startdate DATE NOT NULL,
	PRIMARY KEY(listing_id, civ_id),
	FOREIGN KEY(listing_id)
		REFERENCES listing(listing_id)
		ON DELETE CASCADE,
	FOREIGN KEY(civ_id)
		REFERENCES civilian(civ_id)
		ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS profession (
	prof_id BIGINT UNSIGNED UNIQUE,
	description CHAR(100) UNIQUE NOT NULL,
	PRIMARY KEY(prof_id) );
		
CREATE TABLE IF NOT EXISTS listing_prof (
	listing_id BIGINT UNSIGNED NOT NULL,
	prof_id BIGINT UNSIGNED NOT NULL,
	PRIMARY KEY(listing_id, prof_id),
	FOREIGN KEY(listing_id)
		REFERENCES listing(listing_id)
		ON DELETE CASCADE,
	FOREIGN KEY(prof_id)
		REFERENCES profession(prof_id)
		ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS civ_prof (
	civ_id BIGINT UNSIGNED NOT NULL,
	prof_id BIGINT UNSIGNED NOT NULL,
	PRIMARY KEY(civ_id, prof_id),
	FOREIGN KEY(civ_id)
		REFERENCES civilian(civ_id)
		ON DELETE CASCADE,
	FOREIGN KEY(prof_id)
		REFERENCES profession(prof_id)
		ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS skill (
            skill_id BIGINT UNSIGNED UNIQUE,
	description CHAR(100) UNIQUE NOT NULL,
	PRIMARY KEY(skill_id) );
	
CREATE TABLE IF NOT EXISTS listing_skill (
	listing_id BIGINT UNSIGNED NOT NULL,
	skill_id BIGINT UNSIGNED NOT NULL,
	PRIMARY KEY(listing_id, skill_id),
	FOREIGN KEY(listing_id)
		REFERENCES listing(listing_id)
		ON DELETE CASCADE,
	FOREIGN KEY(skill_id)
		REFERENCES skill(skill_id)
		ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS civ_skill (
	civ_id BIGINT UNSIGNED NOT NULL,
	skill_id BIGINT UNSIGNED NOT NULL,
	PRIMARY KEY(civ_id, skill_id),
	FOREIGN KEY(civ_id)
		REFERENCES civilian(civ_id)
		ON DELETE CASCADE,
	FOREIGN KEY(skill_id)
		REFERENCES skill(skill_id)
		ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS title (
	title_id SERIAL,
	civ_id BIGINT UNSIGNED NOT NULL,
	description CHAR(50) NOT NULL,
	PRIMARY KEY(title_id, civ_id),
	FOREIGN KEY(civ_id)
		REFERENCES civilian(civ_id)
		ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS lang (
	lang_id SERIAL,
	civ_id BIGINT UNSIGNED NOT NULL,
	description CHAR(50) NOT NULL,
	PRIMARY KEY(lang_id, civ_id),
        FOREIGN KEY(civ_id)
		REFERENCES civilian(civ_id)
		ON DELETE CASCADE );
		
CREATE TABLE IF NOT EXISTS account (
	acc_id SERIAL,
	client_id BIGINT UNSIGNED UNIQUE NOT NULL,
	acc_number BIGINT UNSIGNED NOT NULL,
	balance DECIMAL(10, 2) DEFAULT 0,
	PRIMARY KEY(acc_id, client_id),
	FOREIGN KEY(client_id)
		REFERENCES client(client_id)
		ON DELETE CASCADE );
		
CREATE TABLE IF NOT EXISTS address (
	add_id SERIAL,
	client_id BIGINT UNSIGNED UNIQUE NOT NULL,
	city CHAR(50) NOT NULL,
	zip CHAR(16) NOT NULL,
	street CHAR(50) NOT NULL,
	PRIMARY KEY(add_id, client_id),
	FOREIGN KEY(client_id)
		REFERENCES client(client_id)
		ON DELETE CASCADE );
-- Set database
USE headhunters;

-- Preoload skills
INSERT INTO skill(skill_id, description)
	VALUES('0', 'Coffee Machine Handler');
INSERT INTO skill(skill_id, description)
	VALUES('1', 'Verbal Communication');
INSERT INTO skill(skill_id, description)
	VALUES('2', 'Teamwork');
INSERT INTO skill(skill_id, description)
	VALUES('3', 'Commercial Awareness');
INSERT INTO skill(skill_id, description)
	VALUES('4', 'Analysis Investigating');
INSERT INTO skill(skill_id, description)
	VALUES('5', 'Initiative Self Motivation');
INSERT INTO skill(skill_id, description)
	VALUES('6', 'Drive');
INSERT INTO skill(skill_id, description)
	VALUES('7', 'Written Communication');
INSERT INTO skill(skill_id, description)
	VALUES('8', 'Planning Organizing');
INSERT INTO skill(skill_id, description)
	VALUES('9', 'Flexibility');
INSERT INTO skill(skill_id, description)
	VALUES('10', 'Administering Medication');
INSERT INTO skill(skill_id, description)
	VALUES('11', 'Work Pressure');
INSERT INTO skill(skill_id, description)
	VALUES('12', 'Attention to Detail');
INSERT INTO skill(skill_id, description)
	VALUES('13', 'Brainstorming');
INSERT INTO skill(skill_id, description)
	VALUES('14', 'Confronting People');
INSERT INTO skill(skill_id, description)
	VALUES('15', 'Effective Listening Skills');
INSERT INTO skill(skill_id, description)
	VALUES('16', 'Encouraging People');
INSERT INTO skill(skill_id, description)
	VALUES('17', 'Entertaining Others');
INSERT INTO skill(skill_id, description)
	VALUES('18', 'Evaluating Programming');
INSERT INTO skill(skill_id, description)
	VALUES('19', 'Handling Money');

-- Preload professions
INSERT INTO profession(prof_id, description)
	VALUES('0', 'Software Developer');
INSERT INTO profession(prof_id, description)
	VALUES('1', 'Computer System Analyst');
INSERT INTO profession(prof_id, description)
	VALUES('2', 'Information Security Analyst');
INSERT INTO profession(prof_id, description)
	VALUES('3', 'Web Developer');
INSERT INTO profession(prof_id, description)
	VALUES('4', 'Market Research Analyst');
INSERT INTO profession(prof_id, description)
	VALUES('5', 'Marketing Manager');
INSERT INTO profession(prof_id, description)
	VALUES('6', 'IT Assistant');
INSERT INTO profession(prof_id, description)
	VALUES('7', 'Mechanical Enginner');
INSERT INTO profession(prof_id, description)
	VALUES('8', 'IT MAnager');
INSERT INTO profession(prof_id, description)
	VALUES('9', 'Logistician');
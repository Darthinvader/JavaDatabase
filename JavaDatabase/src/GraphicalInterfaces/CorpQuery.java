/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GraphicalInterfaces;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javadatabase.JavaDatabase;
import javadatabase.MatchingQueries;
import javadatabase.accountInfo;
import javax.swing.*;

/**
 *
 * @author mvard
 */
public class CorpQuery extends JFrame {
    ResultSet lids;
    ArrayList<Integer> civids;
    JPanel listings;
    JPanel matches;
    ArrayList<JRadioButton> listing_buttons;
    ArrayList<JRadioButton> match_buttons;
    JButton hire;
    ButtonGroup listing_group;
    ButtonGroup match_group;
    
    public CorpQuery() {
        ResultSet res;
        String query;
        Statement stm;
        int corp_id = accountInfo.getUsername();
        listings = new JPanel();
        matches = new JPanel();
        listings.setLayout(new BoxLayout(listings, BoxLayout.Y_AXIS));
        matches.setLayout(new BoxLayout(matches, BoxLayout.Y_AXIS));
        getContentPane().add(listings);
        getContentPane().add(matches);
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
        setSize(new Dimension(500, 500));
        CorpQueryListListener list_listener = new CorpQueryListListener();
        
        hire = new JButton("Hire");
        hire.addMouseListener(new HireListener());
        
        listing_buttons = new ArrayList<>();
        listing_group = new ButtonGroup();
        try {
            stm = javadatabase.JavaDatabase.connect.createStatement();
            lids = MatchingQueries.getListings(corp_id);
            while(lids.next()) {
                int listing_id = lids.getInt("listing_id");
                query = "SELECT profession.description "
                    + "FROM profession, listing_prof "
                    + "WHERE (listing_prof.listing_id = '" + listing_id + "') AND ("
                        + "listing_prof.prof_id = profession.prof_id)";
                res = stm.executeQuery(query);
                res.next();
                JRadioButton butt = new JRadioButton("(" + listing_id + ")" + res.getString("description"));
                listing_group.add(butt);
                butt.addMouseListener(list_listener);
                listing_buttons.add(butt);
            }
        } catch(SQLException e) {
            Logger.getLogger(Choice.class.getName()).log(Level.SEVERE, null, e);
        }
        
        for(int i = 0; i < listing_buttons.size(); ++i) {
            listings.add(listing_buttons.get(i));
        }
        
        setVisible(true);
    }
    
    public class CorpQueryListListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            JRadioButton source = (JRadioButton) e.getSource();
            String query;
            ResultSet res;
            Statement stm;
            int listing_id = -1;
            int index = listing_buttons.indexOf(source);
            ArrayList<Integer> cids = new ArrayList<>();
            
            match_buttons = new ArrayList<>();
            matches.removeAll();
            match_group = new ButtonGroup();
            
            try {
                stm = javadatabase.JavaDatabase.connect.createStatement();
                if(lids.absolute(index + 1)) listing_id = lids.getInt("listing_id");
                System.out.println("Button: " + source.getText() + " id: "+ listing_id);
                cids = MatchingQueries.MatchCorp(listing_id);
                civids = cids;
                System.out.println("Found " + cids.size() + " matches");
                for(int i = 0; i < cids.size(); ++i) {
                    query = "SELECT name FROM client "
                            + "WHERE client_id = '" + cids.get(i) + "'";
                    res = stm.executeQuery(query);
                    res.next();
                    JRadioButton butt = new JRadioButton("(" + cids.get(i) + ")" + res.getString("name"));
                    match_group.add(butt);
                    match_buttons.add(butt);
                }
                
                for(int i = 0; i < match_buttons.size(); ++i) {
                    matches.add(match_buttons.get(i));
                }
                
                matches.add(hire);
                
            } catch(SQLException ex) {
                Logger.getLogger(Choice.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            repaint();
            revalidate();
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }
    
    public class HireListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            int selected_listing;
            int selected_civ = -1;
            ResultSet res;
            String query;
            Statement stm;
            
            for(int i = 0; i < match_buttons.size(); ++i) {
                if(match_buttons.get(i).isSelected()){
                    selected_civ = civids.get(i);
                    break;
                }
            }
            
            for(int i = 0; i < listing_buttons.size(); ++i) {
                if(listing_buttons.get(i).isSelected()) {
                    try {
                        stm = javadatabase.JavaDatabase.connect.createStatement();
                        //stm = javadatabase.JavaDatabase.connect.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                   //ResultSet.CONCUR_UPDATABLE);
                        lids.absolute(i + 1);
                        selected_listing = lids.getInt("listing_id");
                        System.out.println("selected_id = " + selected_listing + "\nselected_civ = " + selected_civ);
                        query = "UPDATE listing "
                                + "SET filled = '1' "
                                + "WHERE listing_id = '" + selected_listing + "'";
                        stm.executeUpdate(query);
                        query = "UPDATE listing "
                                + "SET filled = '1' "
                                + "WHERE listing_id IN (SELECT "
                                    + "listing_id FROM civ_listing "
                                    + "WHERE civ_id = '" + selected_civ + "')";
                        stm.executeUpdate(query);
                        /*res.next();
                        res.updateInt("filled", 1);
                        res.updateRow();*/
                    } catch(SQLException ex) {
                        Logger.getLogger(Choice.class.getName()).log(Level.SEVERE, null, ex);

                    }
                    break;
                }
            }
            
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
        
    }
}

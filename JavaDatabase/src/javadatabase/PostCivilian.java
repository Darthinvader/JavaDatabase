/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javadatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
/**
 *
 * @author John Psaradakis
 */
public class PostCivilian {
    
    public static int postCivilian(String Languages,String Titles,ArrayList<String> Skills,ArrayList<String> Profession,java.sql.Date date) throws SQLException{
        Statement sqlStatement = JavaDatabase.connect.createStatement();    
        int client_id = accountInfo.getUsername();
        int listing_id = 0;
        ResultSet rs;
        int skill_id,prof_id,user_id;
        user_id = accountInfo.getUsername();
        String statement = "INSERT INTO civilian(civ_id) VALUES('" + client_id + "')";
        sqlStatement.executeUpdate(statement);
        statement = "UPDATE account SET account.balance = '" + (accountInfo.GetMoney() - 10) +"' WHERE account.client_id ='" + user_id + "'";
        sqlStatement.executeUpdate(statement);
        accountInfo.SetMoney(accountInfo.GetMoney() -10);
        statement = "INSERT INTO listing(filled) VALUES('0')";
        sqlStatement.executeUpdate(statement,Statement.RETURN_GENERATED_KEYS);
        rs = sqlStatement.getGeneratedKeys();
        rs.next();
        listing_id = rs.getInt(1);
        //statement = "SELECT listing.listing_id FROM  WHERE client.username ='" + Username+"'"; 
        statement = "INSERT INTO civ_listing(listing_id,civ_id,startdate) VALUES('"+ listing_id +"','" + client_id + "','" + date +"')";
        sqlStatement.executeUpdate(statement);
        statement = "INSERT INTO lang(civ_id,description) VALUES('" + client_id + "','" + Languages +"')";
        sqlStatement.executeUpdate(statement);
        statement = "INSERT INTO title(civ_id,description) VALUES('" + client_id + "','" + Titles +"')";
        sqlStatement.executeUpdate(statement);
        
        for(int i=0;i<Skills.size();i++){
            skill_id = Enum.valueOf(SkillEnum.Skills.class, Skills.get(i)).ordinal();
            System.out.println(Enum.valueOf(SkillEnum.Skills.class, Skills.get(i)).ordinal());
            statement = "INSERT INTO civ_skill(civ_id,skill_id) VALUES('" + client_id + "','" + skill_id +"')";
            sqlStatement.executeUpdate(statement);
        }
        for(int i=0;i<Profession.size();i++){
            prof_id = Enum.valueOf(ProfEnum.Prof.class, Profession.get(i)).ordinal();
            statement = "INSERT INTO civ_prof(civ_id,prof_id) VALUES('" + client_id + "','" + prof_id +"')";
            sqlStatement.executeUpdate(statement);
        }
        return 1;
    }
    
}

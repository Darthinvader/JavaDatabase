/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javadatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 *
 * @author John Psaradakis
 */
public class ConnectToDB {
    public static Connection conn;
    public static Connection getConnection() throws Exception{
        try {
                String Driver = "com.mysql.jdbc.Driver";
                String url = "jdbc:mysql://localhost:3306/headhunters";
                String username = "root";
                String password = "";
                Class.forName(Driver);
                conn = DriverManager.getConnection(url,username,password);
                System.out.println("Connected");
                return conn;
            } catch(ClassNotFoundException | SQLException e){System.out.println(e);}
            
        return null;
    }
}
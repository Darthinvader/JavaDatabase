/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javadatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author John Psaradakis
 */
public class PostJobOffer {
   
    
    public static int PostJobOffer(String city, int hours, ArrayList<String> Professions, 
      ArrayList<String> Skills, java.sql.Date deadline)
      throws SQLException {
        Statement stm = JavaDatabase.connect.createStatement();
        String query;
        ResultSet res;
        int corp_id = accountInfo.getUsername();
        
        query = "SELECT corp_id FROM corporation " +
                    "WHERE corporation.corp_id = '" + corp_id + "'";
        res = stm.executeQuery(query);
        if(!res.next()) {
            query = "INSERT INTO corporation(corp_id)" + 
                        "VALUES('" + corp_id + "')";
            stm.executeUpdate(query);
        }
        
        query = "INSERT INTO listing() VALUES()";
        stm.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
        res = stm.getGeneratedKeys();
        res.next();
        int listing_id = res.getInt(1);
        query = "INSERT INTO corp_listing(listing_id, corp_id, city, deadline, hours)" +
                    "VALUES('" + listing_id + "', '" + corp_id + "', '" + city + "', '" + deadline +
                       "', '" + hours + "')";
        stm.executeUpdate(query);
        int prof_id, skill_id;
        for(int i = 0; i < Professions.size(); ++i) {
            prof_id = Enum.valueOf(ProfEnum.Prof.class, Professions.get(i)).ordinal();
            query = "INSERT INTO listing_prof(listing_id, prof_id) " + 
                        "VALUES('" + listing_id + "', '" + prof_id + "')";
            stm.executeUpdate(query);
        }
        for(int i = 0; i < Skills.size(); ++i) {
            skill_id = Enum.valueOf(SkillEnum.Skills.class, Skills.get(i)).ordinal();
            query = "INSERT INTO listing_skill(listing_id, skill_id) " + 
                        "VALUES('" + listing_id + "', '" + skill_id + "')";
            stm.executeUpdate(query);
        }
        
        return 1;
    }
    
}

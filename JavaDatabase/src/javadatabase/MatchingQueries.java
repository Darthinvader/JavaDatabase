/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javadatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author mvard
 */
public class MatchingQueries {

    public static ArrayList<Integer> MatchCiv() 
      throws SQLException {
        Statement stm = JavaDatabase.connect.createStatement();
        String query;
        ResultSet corpList, res;
        int civ_id = accountInfo.getUsername();
        ArrayList<Integer> validListings = new ArrayList<>();  
        
        //Get all job listing ids
        query = "SELECT listing_id FROM corp_listing";
        corpList = stm.executeQuery(query);
        
        //iterate over ALL job offers and check if the user meets
        //their requirements
        while(corpList.next()) {
            int listing_id = corpList.getInt("listing_id");
            
            //Check if civ matches listing skillset
            query = "SELECT skill_id FROM listing_skill " + 
                    "WHERE listing_id = '" + listing_id + "' AND " +
                    "skill_id NOT IN (SELLECT skill_id FROM civ_skill " +
                        "WHERE civ_id = '" + civ_id + "')";
            res = stm.executeQuery(query);
            //if res not empty then user does not meet skill reqs
            if(res.next()) continue; 
            
            query = "SELECT prof_id FROM listing_prof " + 
                    "WHERE listing_id = '" + listing_id + "' AND " +
                    "prof_id NOT IN (SELLECT prof_id FROM civ_prof " +
                        "WHERE civ_id = '" + civ_id + "')";
            res = stm.executeQuery(query);
            //if res empty then user meets prof reqs
            if(!res.next()) validListings.add(listing_id);
        }
        
        return validListings;
    }
    
    public static ResultSet getListings(int corp_id) 
      throws SQLException {
        Statement stm = JavaDatabase.connect.createStatement();
        String query;
        ResultSet lid;
        
        query = "SELECT listing_id FROM corp_listing "
                + "WHERE corp_id = '" + corp_id + "'";
        lid = stm.executeQuery(query);
        
        return lid;
    }
    
    public static ArrayList<Integer> MatchCorp(int listing_id)
      throws SQLException {
        Statement stm_id = JavaDatabase.connect.createStatement();
        Statement stm_res = JavaDatabase.connect.createStatement();
        String query;
        ResultSet civIDs, res;
        ArrayList<Integer> validCivs = new ArrayList<>();
        
        //get all civs looking for work
        query = "SELECT civ_id FROM civ_listing";
        civIDs = stm_id.executeQuery(query);
        
        while(civIDs.next()) {
            int civ_id = civIDs.getInt("civ_id");
            
            //Check if civ matches listing skillset
            query = "SELECT skill_id FROM listing_skill "
                    + "WHERE listing_id = '" + listing_id + "' AND "
                    + "skill_id NOT IN (SELECT skill_id FROM civ_skill " 
                    + "WHERE civ_id = '" + civ_id + "')";
            res = stm_res.executeQuery(query);
            //if res not empty then user does not meet skill reqs
            if(res.next()) continue; 
            
            query = "SELECT prof_id FROM listing_prof "
                    + "WHERE listing_id = '" + listing_id + "' AND " 
                    + "prof_id NOT IN (SELECT prof_id FROM civ_prof " 
                    + "WHERE civ_id = '" + civ_id + "')";
            res = stm_res.executeQuery(query);
            //if res empty then user meets prof reqs
            if(!res.next()) validCivs.add(civ_id);
        }
        
        return validCivs;
    }
}

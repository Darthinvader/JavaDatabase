/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javadatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


/**
 *
 * @author John Psaradakis
 */
public class LogIn {
    /**
     *
     * @param Username
     * @param Password
     * @return 1 if success or 0 if failure
     * @throws java.sql.SQLException
     */
    public static int login(String Username,String Password) throws SQLException{
        Statement sqlStatement = JavaDatabase.connect.createStatement();
        String statement = "SELECT client.client_id FROM client WHERE client.username ='" + Username + "' AND client.password = '" + Password+ "'";  
        ResultSet Query = sqlStatement.executeQuery(statement);
        if(!Query.next()){
            JOptionPane.showMessageDialog(new JFrame(), "Wrong password or Username");
            return 0;
        }
        else{
            int user_id = Query.getInt("client_id");
            statement = "SELECT account.balance FROM account WHERE account.client_id ='" + user_id + "'";
            Query = sqlStatement.executeQuery(statement);
            Query.next();
            accountInfo.SetMoney(Query.getInt("balance"));
            accountInfo.SetUsername(user_id);
            return 1;
        }
    }
    
}

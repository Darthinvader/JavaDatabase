/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javadatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author John Psaradakis
 */
public class SignUp {
    
    /**
     *
     * @param Username
     * @param Password
     * @param Phone
     * @param Email
     * @param Name
     * @param City
     * @param ZipCode
     * @param Street
     * @param Account_number
     * @param Money
     * @return
     * @throws SQLException
     */
    public static int Signup(String Username,String Password,String Phone,
            String Email,String Name,String City,String ZipCode,String Street,int Account_number,float Money) throws SQLException{
        Statement sqlStatement = JavaDatabase.connect.createStatement();
        String statement = "SELECT client.client_id FROM client WHERE client.username ='" + Username+"'";  
        ResultSet Query = sqlStatement.executeQuery(statement);
        System.out.println();
        if(Query.next()){JOptionPane.showMessageDialog(new JFrame(), "Username Taken");return 0;}
        statement = "SELECT client.client_id FROM client WHERE client.email ='" + Email +"'";
        Query = sqlStatement.executeQuery(statement);
        if(Query.next()){JOptionPane.showMessageDialog(new JFrame(), "Username Taken");return 0;}
        statement = "INSERT INTO client(username, password,email,phone,name) VALUES('" + Username + "','" + Password + "','" + Email + "', '" +Phone + "', '" +Name +"')";
        sqlStatement.executeUpdate(statement);
        statement = "SELECT client.client_id FROM client WHERE client.username = '" + Username + "'";
        Query = sqlStatement.executeQuery(statement);
        Query.next();
        int client_id = Query.getInt("client_id");
        statement = "INSERT INTO account(client_id,acc_number,balance) VALUES('" + client_id+ "','"+Account_number + "','" + Money +"')";
        sqlStatement.executeUpdate(statement);
        statement = "INSERT INTO address(client_id,city,zip,street) VALUES('" + client_id+ "','"+City +"','"+ ZipCode + "','" + Street +"')";
        sqlStatement.executeUpdate(statement);
        accountInfo.SetUsername(client_id);
        accountInfo.SetMoney(Money);
        return 1;
    }
}
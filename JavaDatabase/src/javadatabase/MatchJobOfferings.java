/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javadatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

/**
 *
 * @author John Psaradakis
 */
public class MatchJobOfferings {
    public static boolean MatchTables(ArrayList<Integer> SP,ArrayList<Integer> SPC){
        boolean flag;
        if(SP.size()>SPC.size()){return false;}
        for(int i=0;i<SP.size();i++){
            flag = false;
            for(int j = 0;j<SPC.size();j++){
                if(Objects.equals(SP.get(i), SPC.get(j))){flag = true;break;}
            }
            if(flag == false){return false;}
        }
        return true;
    }
    public static ArrayList<Integer> GetProfession(ResultSet rs) throws SQLException{
        ArrayList<Integer> Profession = new ArrayList<>();
        while(rs.next()){
            Profession.add(rs.getInt("prof_id"));        
        }
        return Profession;
    }
    public static ArrayList<Integer> GetSkills(ResultSet rs) throws SQLException{
        ArrayList<Integer> Skills = new ArrayList<>();
        while(rs.next()){
            Skills.add(rs.getInt("skill_id"));        
            }
        return Skills;
    }
    
    public static String Matcher() throws SQLException{
        String statement = "SELECT * FROM corp_listing";
        Statement sqlStatement = JavaDatabase.connect.createStatement();
        Statement sqlStatement2 = JavaDatabase.connect.createStatement();
        Statement sqlStatementc = JavaDatabase.connect.createStatement();
        Statement sqlStatementp = JavaDatabase.connect.createStatement();
        ResultSet rs = sqlStatement.executeQuery(statement);
        ResultSet rs2,rsc,rsp;
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        java.sql.Date date1,datesiv;
        boolean filled;
        ArrayList<Integer> Professions = new ArrayList<>();
        ArrayList<Integer> Skills = new ArrayList<>();
        ArrayList<Integer> Professionsc = new ArrayList<>();
        ArrayList<Integer> Skillsc = new ArrayList<>();
        String Matches="The matches are: \n";
        int corp_id,civ_id,listing_id,listing_idc;
        String corp_name,civ_name;
        while(rs.next()){
            System.out.println("Hello");
            Skills.clear();
            Professions.clear();
            listing_id = rs.getInt("listing_id");
            statement = "SELECT * FROM listing WHERE listing.listing_id = '" + listing_id +"'";
            rs2 = sqlStatement2.executeQuery(statement);
            rs2.next();
            filled = (rs2.getInt("filled") == 1);
            if(filled == true){
                System.out.println("its filled:P");
                continue;
            }
            date1 = rs.getDate("deadline");
            if(date1.compareTo(date) <= 0){
                System.out.println("fuck my life dates :P");
                continue;
            }
            corp_id = rs.getInt("corp_id");
            statement = "SELECT * FROM client WHERE client.client_id = '" + corp_id +"'"; 
            rs2 = sqlStatement2.executeQuery(statement);
            rs2.next();
            corp_name = rs2.getString("name");
            Matches += "The corporation " + corp_name + " has the following civilian to take for the job :\n";
            statement = "SELECT * FROM listing_prof WHERE listing_prof.listing_id = '" + listing_id +"'";
            rs2 = sqlStatement2.executeQuery(statement);
            Professions = MatchJobOfferings.GetProfession(rs2);
            statement = "SELECT * FROM listing_skill WHERE listing_skill.listing_id = '" + listing_id +"'";
            rs2 = sqlStatement2.executeQuery(statement);
            Skills = MatchJobOfferings.GetSkills(rs2);
            statement = "SELECT * FROM civ_listing";
            rsc = sqlStatementc.executeQuery(statement);
            System.out.println("Here???");
            while(rsc.next()){
                System.out.println("The fuck is going on");
                civ_id = rsc.getInt("civ_id");
                statement = "SELECT * FROM client WHERE client.client_id = '" + civ_id +"'";
                rsp = sqlStatementp.executeQuery(statement);
                rsp.next();
                civ_name = rsp.getString("name");
                Skillsc.clear();
                Professionsc.clear();
                listing_idc = rsc.getInt("listing_id");
                datesiv = rsc.getDate("startdate");
                if(datesiv.compareTo(date) >=0){
                    continue;
                }
                statement = "SELECT * FROM civ_prof WHERE civ_prof.civ_id ='" + civ_id + "'";
                rsp = sqlStatementp.executeQuery(statement);
                Professionsc = MatchJobOfferings.GetProfession(rsp);
                statement = "SELECT * FROM civ_skill WHERE civ_skill.civ_id = '" + civ_id + "'";
                rsp = sqlStatementp.executeQuery(statement);
                Skillsc = MatchJobOfferings.GetSkills(rsp);
                if(MatchJobOfferings.MatchTables(Skills, Skillsc) == true && MatchJobOfferings.MatchTables(Professions,Professionsc) ==true){
                    Matches += civ_name +" has all the qualifications \n";
                }
            }
        }       
        return Matches;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javadatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author John Psaradakis
 */


public class HistoryMaker {
   
    private static String Corp_History(ResultSet rs,String name) throws SQLException{
        String string ="";
        int flag;
        while(rs.next()){
            flag = rs.getInt("filled");
            if(flag == 1){
                string += "The corporation " + name +  " has completed a job offer \n";
            }
        }
        return string;
    }
    
    public static String Civ_History(ResultSet rs,String name) throws SQLException{
        String string ="";
        int flag;
        while(rs.next()){
            flag = rs.getInt("filled");
            if(flag == 1){
                string += "The civilian " + name +  " has been contracted to a job \n";
            }
        }
        return string;
    
    }
    
    public static String HistoryMaker() throws SQLException{
        String History = "History is as follows :\n";
        Statement sqlStatement = JavaDatabase.connect.createStatement();
        Statement sqlStatement2 = JavaDatabase.connect.createStatement();
        String statement = "SELECT * FROM corp_listing";
        ResultSet rs = sqlStatement.executeQuery(statement);
        ResultSet rs2;
        History += "The Corporations that have completed Job Offers are : \n";
        String Corporation,Name;
        int listing_id,client_id;
        while(rs.next()){
            listing_id = rs.getInt("listing_id");
            client_id = rs.getInt("corp_id");
            statement = "SELECT * FROM client WHERE client.client_id = '" + client_id +"'";
            rs2 = sqlStatement2.executeQuery(statement);
            rs2.next();
            Name = rs2.getString("name");
            statement = "SELECT * FROM listing WHERE listing_id = '" + listing_id +"'";
            rs2 = sqlStatement2.executeQuery(statement);
            History+=HistoryMaker.Corp_History(rs2, Name);
        }
        History += "\n \nThe cilivians that have been contracted to a Job are : \n";
        statement = "SELECT * FROM civ_listing";
        rs = sqlStatement.executeQuery(statement);
        while(rs.next()){
            listing_id = rs.getInt("listing_id");
            client_id = rs.getInt("civ_id");
            statement = "SELECT * FROM client WHERE client.client_id = '" + client_id +"'";
            rs2 = sqlStatement2.executeQuery(statement);
            rs2.next();
            Name = rs2.getString("name");
            statement = "SELECT * FROM listing WHERE listing_id ='" + listing_id + "'";
            rs2 = sqlStatement2.executeQuery(statement);
            History += HistoryMaker.Civ_History(rs2, Name);
        }
        return History;
    }
}
